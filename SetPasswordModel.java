import java.lang.Override;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * Set Password Model
 */
public class SetPasswordModel {


    private String newPassword;

    /**
     * Getter for newPassword
     *
     * New Password
     */
    public String getNewPassword() {
        return this.newPassword;
    }

    /**
     * Setter for newPassword
     *
     * New Password
     */
    public void setNewPassword(String value) {
        this.newPassword = value;
    }


    /**
     * Returns a JSON string representation of SetPasswordModel
     */
    @Override
    public String toString() {
        return JsonSerializer.SerializeObject(this);
    }
}