
import java.lang.Override;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * Represents information about a newly created account
 */

public class NewAccountModel {


    private Integer accountId;

    /**
     * Getter for accountId
     *
     * This is the ID number of the account that was created
     */
    public Integer getAccountId() {
        return this.accountId;
    }

    /**
     * Setter for accountId
     *
     * This is the ID number of the account that was created
     */
    public void setAccountId(Integer value) {
        this.accountId = value;
    }


    private String accountDetailsEmailedTo;

    /**
     * Getter for accountDetailsEmailedTo
     *
     * This is the email address to which credentials were mailed
     */
    public String getAccountDetailsEmailedTo() {
        return this.accountDetailsEmailedTo;
    }

    /**
     * Setter for accountDetailsEmailedTo
     *
     * This is the email address to which credentials were mailed
     */
    public void setAccountDetailsEmailedTo(String value) {
        this.accountDetailsEmailedTo = value;
    }


    private Date createdDate;

    /**
     * Getter for createdDate
     *
     * The date and time when this account was created
     */
    public Date getCreatedDate() {
        return this.createdDate;
    }

    /**
     * Setter for createdDate
     *
     * The date and time when this account was created
     */
    public void setCreatedDate(Date value) {
        this.createdDate = value;
    }


    private String limitations;

    /**
     * Getter for limitations
     *
     * If this account includes any limitations, specify them here
     */
    public String getLimitations() {
        return this.limitations;
    }

    /**
     * Setter for limitations
     *
     * If this account includes any limitations, specify them here
     */
    public void setLimitations(String value) {
        this.limitations = value;
    }


    private String paymentUrl;

    /**
     * Getter for paymentUrl
     *
     * The payment url where the payment method can be set up
     */
    public String getPaymentUrl() {
        return this.paymentUrl;
    }

    /**
     * Setter for paymentUrl
     *
     * The payment url where the payment method can be set up
     */
    public void setPaymentUrl(String value) {
        this.paymentUrl = value;
    }


    /**
     * Returns a JSON string representation of NewAccountModel
     */
    @Override
    public String toString() {
        return JsonSerializer.SerializeObject(this);
    }
}