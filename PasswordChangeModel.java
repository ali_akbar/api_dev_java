import java.lang.Override;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Password Change Model
 */

public class PasswordChangeModel {


    private String oldPassword;

    /**
     * Getter for oldPassword
     *
     * Old Password
     */
    public String getOldPassword() {
        return this.oldPassword;
    }

    /**
     * Setter for oldPassword
     *
     * Old Password
     */
    public void setOldPassword(String value) {
        this.oldPassword = value;
    }


    private String newPassword;

    /**
     * Getter for newPassword
     *
     * New Password
     */
    public String getNewPassword() {
        return this.newPassword;
    }

    /**
     * Setter for newPassword
     *
     * New Password
     */
    public void setNewPassword(String value) {
        this.newPassword = value;
    }


    /**
     * Returns a JSON string representation of PasswordChangeModel
     */
    @Override
    public String toString() {
        return JsonSerializer.SerializeObject(this);
    }
}